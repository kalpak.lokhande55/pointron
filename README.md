# App URLs

Live: https://app.pointron.io

Preview: https://pre.pointron.io

Dev: https://dev.pointron.io

# To run locally

1. Clone the repo

   ```
   git clone https://gitlab.com/blanklabs/tidigit/pointron.git

   git clone --recurse-submodules https://gitlab.com/blanklabs/tidigit/pointron.git

   ```

2. Initialize the submodule if `--recursive-submodules` flag is not used in the first step
   ```
   git submodule update --init --recursive
   ```
3. Install dependencies
   ```
   cd <folder name>
   npm install
   ```
4. Initialize `.env` file with secrets
5. Run the app
   ```
   npm run dev
   ```
