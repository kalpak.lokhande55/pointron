import { handleErrorWithSentry, Replay } from "@sentry/sveltekit";
import * as Sentry from "@sentry/sveltekit";

// Sentry.init({
//   dsn: 'https://90d1db824300f84af6ac138215b8736f@o4506479439773696.ingest.sentry.io/4506479445868544',
//   tracesSampleRate: 1.0,

//   // This sets the sample rate to be 10%. You may want this to be 100% while
//   // in development and sample at a lower rate in production
//   replaysSessionSampleRate: 0.1,

//   // If the entire session is not sampled, use the below sample rate to sample
//   // sessions when an error occurs.
//   replaysOnErrorSampleRate: 1.0,

//   // If you don't want to use Session Replay, just remove the line below:
//   integrations: [new Replay()],
// });

// // If you have a custom error handler, pass it to `handleErrorWithSentry`
// export const handleError = handleErrorWithSentry();

export async function handleError({ error, event }) {
  // You can log the error or send it to a logging service here
  console.error(error);

  // Customize the error object
  // For example, set a custom error message
  return new Error("Custom error message: " + error);
}
