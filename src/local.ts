import { LocalDexie } from "./stores/local.dexie";

const appName = "Pointron";
const defaultAppMenu: string[] = ["focus", "journal", "goal", "analytics"];

const defaultAppData = {
  name: appName,
  version: "0.0.1",
  leftPanelFooter: "simple",
  homePath: "focus",
  help: [
    {
      section: "main",
      children: [
        "tutorial",
        "productguide",
        "chat",
        "call",
        "faqs",
        "downloads"
      ]
    },
    {
      section: "Product direction",
      children: ["changelog", "roadmap", "feedback", "requestfeature", "report"]
    },
    {
      section: "Community",
      children: ["discord", "opencollective", "twitter"]
    },
    {
      section: "Learn more",
      children: ["about", "privacy", "git", "credits"]
    }
  ],
  cp: [
    {
      children: ["shortcuts", "accessibility", "importexport"],
      order: 3,
      section: "app"
    },
    {
      children: [
        "theme",
        "session-settings",
        "analytics-settings",
        "presets",
        "datetime-settings",
        "targets",
        "alerts-disabled"
      ],
      order: 2,
      section: "customization"
    },
    {
      children: ["tags", "cplogs"],
      isHideTitle: true,
      order: 1,
      orientation: "vertical",
      section: "modules",
      visibility: true
    },
    {
      children: [
        "about",
        "share",
        "productguide",
        "feedback",
        "privacy",
        "discord",
        "troubleshoot"
      ],
      isHideTitle: true,
      order: 4,
      section: "other"
    }
  ]
};

export { LocalDexie, defaultAppData, defaultAppMenu };
