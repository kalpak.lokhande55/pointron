/** @type {import('tailwindcss').Config} */
module.exports = {
  presets: [require("./lib/client/theme/tidigit.tailwind.cjs")]
};
