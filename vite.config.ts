import { sentrySvelteKit } from "@sentry/sveltekit";
import { sveltekit } from "@sveltejs/kit/vite";
import type { UserConfig } from "vite";

const config: UserConfig = {
  // plugins: [sentrySvelteKit({
  //     sourceMapsUploadOptions: {
  //         org: "blank-labs-private-limited",
  //         project: "javascript-sveltekit"
  //     }
  // }), sveltekit()]
  plugins: [sveltekit()]
};

export default config;
